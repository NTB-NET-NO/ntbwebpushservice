﻿// -----------------------------------------------------------------------
// <copyright file="WebPushException.cs" company="NTB">
// NTB
// </copyright>
// -----------------------------------------------------------------------

namespace NTB.WebPush.Components.Exceptions
{
    using System;
   
    /// <summary>
    /// TODO: Update summary.
    /// </summary>
    public class WebPushException : Exception
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="WebPushException"/> class.
        /// </summary>
        /// <remarks>Specifies an error message only.</remarks>
        /// <param name="message">The error message</param>
        public WebPushException(string message)
            : base(message)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="WebPushException"/> class.
        /// </summary>
        /// <remarks>
        /// Allows for specification of both an error message and an inner exception
        /// </remarks>
        /// <param name="message">The error message.</param>
        /// <param name="exception">Inner exception to wrap.</param>
        public WebPushException(string message, Exception exception)
            : base(message, exception)
        {
        }
    }
}