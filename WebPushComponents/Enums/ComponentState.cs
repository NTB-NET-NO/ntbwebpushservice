// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ComponentState.cs" company="NTB">
//   NTB
// </copyright>
// <summary>
//   The component state.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace NTB.WebPush.Components.Enums
{
    /// <summary>
    /// The component state.
    /// </summary>
    public enum ComponentState
    {
        /// <summary>
        /// This value is just used to tell if the component is in started modus (running)
        /// </summary>
        Running, 

        /// <summary>
        /// this value is just used to tell if the component is in halted mode
        /// </summary>
        Halted
    }
}