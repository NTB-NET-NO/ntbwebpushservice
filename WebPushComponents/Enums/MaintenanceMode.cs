﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="MaintenanceMode.cs" company="NTB">
//   NTB
// </copyright>
// <summary>
//   The maintenance mode.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace NTB.WebPush.Components.Enums
{
    /// <summary>
    /// The maintenance mode.
    /// </summary>
    public enum MaintenanceMode
    {
        /// <summary>
        /// CheckMatches mode means that we are checking for matches that has caused us problems earlier in the day
        /// </summary>
        CheckMatches, 

        /// <summary>
        /// CheckAllMatches mode means that we are checking for matches that has caused us problems in the past
        /// </summary>
        CheckAllMatches
    }
}