﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="PushComponent.cs" company="NTB">
//   NTB
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace NTB.WebPush.Components
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Configuration;
    using System.IO;
    using System.Threading;
    using System.Xml;

    using log4net;

    // Support for Enums
    using NTB.WebPush.Components.Enums;

    // Support for logging
    using NTB.WebPush.Components.Exceptions;

    // Support for interface
    using NTB.WebPush.Components.Interfaces;
    
    /// <summary>
    /// The push component.
    /// </summary>
    public partial class PushComponent : Component, IBaseWebPush
    {
        /// <summary>
        /// The Configured interval for this instance, used for Continous polling
        /// </summary>
        /// <remarks>
        ///   <para>Indicates the interval time in seconds for <c>Continous</c>polling. 60 means that the job runs every minute.</para>
        ///   <para>Default value: <c>60</c></para>
        /// </remarks>
        protected int Interval = 60;

        /// <summary>
        /// The Configured job name for this instance
        /// </summary>
        /// <remarks>Internal field, accessed through interface implemenation <see cref="InstanceName"/></remarks>
        protected string Name;

        /// <summary>
        /// Flag to indicate sucessful configure. Instance will not start polling nor handle messages if not properly Configured
        /// </summary>
        /// <remarks>Holds the Configured status of the instance. <c>True</c> means successfully Configured</remarks>
        protected bool Configured = false;

        /// <summary>
        /// Error-Retry flag
        /// </summary>
        /// <remarks>The flag is being set if a network or EWS error is encountered. Current operations are being aborted for later retry.</remarks>
        protected bool ErrorRetry = false;

        /// <summary>
        /// Send Email-notifications when a new messages is processed
        /// </summary>
        /// <remarks>Set to an email address. Multiple adresses are supported, separate with <c>;</c></remarks>
        protected string EmailNotification;

        /// <summary>
        /// Subject for email notifications
        /// </summary>
        /// <remarks>The subject is built during processing and used when sending the email when processing completes</remarks>
        protected string EmailSubject;

        /// <summary>
        /// Body for email notifications
        /// </summary>
        /// <remarks>The body is built during processing and used when sending the email when processing completes</remarks>
        protected string EmailBody;

        /// <summary>
        /// The schedule type.
        /// </summary>
        protected ScheduleType ScheduleType;

        /// <summary>
        /// Error file folder
        /// </summary>
        /// <remarks>
        ///   <para>File folder where failing files are stored.</para>
        ///   <para>If this is not set, failing files are deleted instead of saved.</para>
        /// </remarks>
        protected string FileErrorFolder;

        /// <summary>
        /// File folder where completed files are stored
        /// </summary>
        /// <remarks>
        ///   <para>File folder where completed files are archived. Subdirectories for years, months and dates are created.</para>
        ///   <para>If this is not set, imported files are deleted instead of archived.</para>
        /// </remarks>
        protected string FileDoneFolder;

        /// <summary>
        /// Output file folder
        /// </summary>
        /// <remarks>
        /// File folder where incoming files are read from. Inputs can be modified by <see cref="FileFilter"/> and <see cref="IncludeSubdirs"/>
        /// </remarks>
        protected string FileInputFolder;

        /// <summary>
        /// Input file filter
        /// </summary>
        /// <remarks>
        ///   <para>Defines what file types are being read from <see cref="FileInputFolder"/>.</para>
        ///   <para>Default value: <c>NTBIDType.None</c></para>
        /// </remarks>
        protected string FileFilter = "*.xml";

        /// <summary>
        /// Watch subdirs for new files as well
        /// </summary>
        /// <remarks>
        ///   <para>Defines if the instance should look in subdirectories within <see cref="FileInputFolder"/>.</para>
        ///   <para>Default value: <c>False</c></para>
        /// </remarks>
        protected bool IncludeSubdirs = false;

        /// <summary>
        /// Buffer size for the file watcher
        /// </summary>
        /// <remarks>
        ///   <para>Defines the internal buffer size the FileSystemWatcher when using this polling.
        /// Should not be set to high. The instance will fall back to contious polling on buffer overflows.</para>
        ///   <para>Default value: <c>4096</c></para>
        /// </remarks>
        protected int BufferSize = 4096;

        /// <summary>
        /// PollDelay is used to make sure that the file is ready to be accessed. 
        /// </summary>
        protected int PollDelay = 0;

        /// <summary>
        /// Static logger
        /// </summary>
        private static readonly ILog Logger = LogManager.GetLogger(typeof(PushComponent));

        /// <summary>
        /// Busy status event
        /// </summary>
        private readonly AutoResetEvent _busyEvent = new AutoResetEvent(true);

        /// <summary>
        /// Exit control event
        /// </summary>
        private readonly AutoResetEvent _stopEvent = new AutoResetEvent(false);

        /// <summary>
        /// Initializes static members of the <see cref="PushComponent"/> class. 
        /// </summary>
        static PushComponent()
        {
            // Set up logger
            log4net.Config.XmlConfigurator.Configure();
            if (!LogManager.GetRepository().Configured)
            {
                log4net.Config.BasicConfigurator.Configure();
            }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="PushComponent"/> class.
        /// </summary>
        public PushComponent()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="PushComponent"/> class.
        /// </summary>
        /// <param name="container">
        /// The container.
        /// </param>
        public PushComponent(IContainer container)
        {
            container.Add(this);

            InitializeComponent();
        }

        /// <summary>
        /// Gets a value indicating whether enabled.
        /// </summary>
        /// <exception cref="NotImplementedException">
        /// if not implemented we will stop
        /// </exception>
        public bool Enabled
        {
            get
            {
                return enabled;
            }
        }

        /// <summary>
        /// Gets the name of the instance.
        /// </summary>
        /// <value>The name of the instance.</value>
        /// <remarks>The Configured instance job name.</remarks>
        public string InstanceName
        {
            get
            {
                return Name;
            }
        }

        /// <summary>
        /// Gets the operation mode. 
        /// </summary>
        /// <value>The operation mode.</value>
        /// <remarks><c>Gatherer</c> is the only valid mode, its hard coded for this component.</remarks>
        public OperationMode OperationMode
        {
            get
            {
                return OperationMode.Distributor;
            }
        }

        /// <summary>
        /// Gets the poll style.
        /// </summary>
        /// <value>The poll style.</value>
        /// <remarks>Contionous, Scheduled and FileSystemWatch are valid for <c>Gatherer</c> objects.</remarks>
        public PollStyle PollStyle
        {
            get
            {
                return pollStyle;
            }
        }

        /// <summary>
        /// Gets the component state.
        /// </summary>
        public ComponentState ComponentState
        {
            get
            {
                return componentState;
            }
        }

        /// <summary>
        /// Gets or sets the push target.
        /// </summary>
        protected string PushTarget { get; set; }

        /// <summary>
        /// Gets or sets the push protocol.
        /// </summary>
        protected string PushProtocol { get; set; }

        /// <summary>
        /// Gets or sets the push protocol.
        /// </summary>
        protected int PushPort { get; set; }

        /// <summary>
        /// The configure.
        /// </summary>
        /// <param name="configNode">
        /// The config node.
        /// </param>
        /// <exception cref="NotImplementedException">
        /// if not implemented we will stop
        /// </exception>
        public void Configure(XmlNode configNode)
        {
            // Basic configuration sanity check
            if (configNode.Attributes != null
                && (configNode == null || configNode.Name != "PushComponent"
                    || configNode.Attributes.GetNamedItem("Name") == null))
            {
                throw new ArgumentException("The XML configuration node passed is invalid", "configNode");
            }

            Logger.Debug("Node: " + configNode.Name);
            if (configNode.Attributes != null)
            {
                Logger.Debug("Name attribut: " + configNode.Attributes.GetNamedItem("Name"));

                try
                {
                    Name = configNode.Attributes["Name"].Value;

                    ThreadContext.Stacks["NDC"].Push(InstanceName);
                }
                catch (Exception ex)
                {
                    ThreadContext.Stacks["NDC"].Pop();
                    Logger.Fatal("Not possible to configure this job instance!", ex);
                    throw new ArgumentException("Could not find name of Component", ex);
                }

                try
                {
                    enabled = Convert.ToBoolean(configNode.Attributes["Enabled"].Value);
                }
                catch (Exception ex)
                {
                    ThreadContext.Stacks["NDC"].Pop();
                    Logger.Fatal("Not possible to configure this job instance!", ex);
                    throw new ArgumentException("Could not enable or disable job", ex);
                }

                // Getting Operation Mode value
                try
                {
                    operationMode =
                        (OperationMode)
                        Enum.Parse(typeof(OperationMode), configNode.Attributes["OperationMode"].Value, true);
                }
                catch (Exception ex)
                {
                    ThreadContext.Stacks["NDC"].Pop();
                    throw new ArgumentException("Invalid or missing OperationMode values in XML configuration node", ex);
                }

                // Getting Poll Style value
                try
                {
                    this.pollStyle = (PollStyle)Enum.Parse(typeof(PollStyle), configNode.Attributes["PollStyle"].Value, true);
                }
                catch (Exception ex)
                {
                    ThreadContext.Stacks["NDC"].Pop();
                    throw new ArgumentException("Invalid or missing PollStyle values in XML configuration node", ex);
                }

                // Getting Buffer Size value
                try
                {
                    BufferSize = Convert.ToInt32(configNode.Attributes["BufferSize"].Value);
                }
                catch (Exception exception)
                {
                    ThreadContext.Stacks["NDC"].Pop();
                    throw new ArgumentException("Invalid or missing BufferSize values in XML configuration node", exception);
                }

                // Getting File Filter value
                try
                {
                    FileFilter = configNode.Attributes["FileFilter"].Value;
                }
                catch (Exception exception)
                {
                    ThreadContext.Stacks["NDC"].Pop();
                    throw new ArgumentException("Invalid or missing FileFilter values in XML configuration node", exception);
                }

                // Getting Poll Delay value
                try
                {
                    PollDelay = Convert.ToInt32(configNode.Attributes["Delay"].Value);
                }
                catch (Exception exception)
                {
                    ThreadContext.Stacks["NDC"].Pop();
                    throw new ArgumentException("Invalid or missing PollDelay values in XML configuration node", exception);
                }
            }

            // Getting values specific to this service and component
            try
            {
                // Find the file folder to work with
                XmlNode folderNode = configNode.SelectSingleNode("InputFolder");

                if (folderNode != null && folderNode.Attributes != null)
                {
                    FileInputFolder = folderNode.InnerText;
                }

                Logger.InfoFormat(
                    "Converter job - Polling: {0} / File input folder: {1} / Enabled: {2}",
                    Enum.GetName(typeof(PollStyle), pollStyle),
                    FileInputFolder,
                    enabled);
            }
            catch (Exception exception)
            {
                Logger.Fatal("Not possible to get input-folder", exception);
                ThreadContext.Stacks["NDC"].Pop();
                throw new ArgumentException("Cannot run job without an input folder", exception);
            }

            // adding the done folder
            try
            {
                // Find the file folder to work with
                XmlNode folderNode = configNode.SelectSingleNode("DoneFolder");
                if (folderNode != null)
                {
                    FileDoneFolder = folderNode.InnerText;
                }

                DirectoryInfo directoryInfo = new DirectoryInfo(FileDoneFolder);
                if (!directoryInfo.Exists)
                {
                    directoryInfo.Create();
                }

                Logger.InfoFormat(
                    "Converter job - Polling: {0} / File done folder: {1} / Enabled: {2}",
                    Enum.GetName(typeof(PollStyle), pollStyle),
                    FileDoneFolder,
                    enabled);
            }
            catch (Exception ex)
            {
                Logger.Fatal("Not possible to get Done-folder", ex);
            }

            // adding the error folder
            try
            {
                // Find the file folder to work with
                XmlNode folderNode = configNode.SelectSingleNode("ErrorFolder");
                if (folderNode != null)
                {
                    FileErrorFolder = folderNode.InnerText;
                }

                DirectoryInfo directoryInfo = new DirectoryInfo(FileErrorFolder);
                if (!directoryInfo.Exists)
                {
                    directoryInfo.Create();
                }

                Logger.InfoFormat(
                    "Converter job - Polling: {0} / File error folder: {1} / Enabled: {2}",
                    Enum.GetName(typeof(PollStyle), pollStyle),
                    FileErrorFolder,
                    enabled);
            }
            catch (Exception ex)
            {
                Logger.Fatal("Not possible to get Done-folder", ex);
            }

            // Getting values specific to this service and component
            try
            {
                // Find the file folder to work with
                XmlNode targetNode = configNode.SelectSingleNode("PushTarget");

                if (targetNode != null && targetNode.Attributes != null)
                {
                    PushTarget = targetNode.Attributes["Url"].Value;
                    PushPort = Convert.ToInt32(targetNode.Attributes["Port"].Value);
                    PushProtocol = targetNode.Attributes["Protocol"].Value;
                }
            }
            catch (Exception exception)
            {
                Logger.Fatal("Not possible to get target for push", exception);
            }

            try
            {
                // Switch on pollstyle
                switch (pollStyle)
                {
                    case PollStyle.Continous:
                        Interval = Convert.ToInt32(configNode.Attributes["Interval"].Value);
                        if (Interval == 0)
                        {
                            Interval = 5000;
                        }

                        pollTimer.Interval = Interval * 1000; // short startup - interval * 1000;
                        Logger.DebugFormat("Poll interval: {0} seconds", Interval);

                        break;

                    case PollStyle.Scheduled:

                        /* 
                        schedule = configNode.Attributes["Schedule"].Value;
                        TimeSpan s = Utilities.GetScheduleInterval(schedule);
                        logger.DebugFormat("Schedule: {0} Calculated time to next run: {1}", schedule, s);
                        pollTimer.Interval = s.TotalMilliseconds;
                        */
                        throw new NotSupportedException("Invalid polling style for this job type");

                    case PollStyle.FileSystemWatch:

                        // Check for config overrides
                        if (configNode.Attributes.GetNamedItem("BufferSize") != null)
                        {
                            BufferSize = Convert.ToInt32(configNode.Attributes["BufferSize"].Value);
                        }

                        Logger.DebugFormat("FileSystemWatcher buffer size: {0}", BufferSize);

                        // Set values
                        filesWatcher.Path = FileInputFolder;
                        filesWatcher.Filter = FileFilter;
                        filesWatcher.InternalBufferSize = BufferSize;

                        // Do not start the event watcher here, wait until after the startup cleaning job
                        pollTimer.Interval = 5000; // short startup;

                        break;

                    default:

                        // Unsupported pollstyle for this object
                        throw new NotSupportedException("Invalid polling style for this job type");
                }
            }
            catch (Exception ex)
            {
                ThreadContext.Stacks["NDC"].Pop();
                throw new ArgumentException(
                    "Invalid or missing PollStyle-specific values in XML configuration node: " + ex.Message, ex);
            }

            // Finish configuration
            ThreadContext.Stacks["NDC"].Pop();
            Configured = true;
        }

        /// <summary>
        /// The start.
        /// </summary>
        /// <exception cref="NotImplementedException">
        /// if not implemented we will stop
        /// </exception>
        public void Start()
        {
            if (!Configured)
            {
                throw new WebPushException("PushComponent is not properly Configured. PurgeComponent::Start() Aborted!");
            }

            if (!enabled)
            {
                throw new WebPushException("PushComponent is not Enabled. PurgeComponent::Start() Aborted!");
            }

            _stopEvent.Reset();
            _busyEvent.Set();

            filesWatcher.Path = FileInputFolder;

            filesWatcher.Filter = FileFilter;

            filesWatcher.InternalBufferSize = BufferSize;
            
            // Starting poll-timer
            
            // pollTimer.Interval = 2000;
            componentState = ComponentState.Running;
            pollTimer.Start();
        }

        /// <summary>
        /// The stop.
        /// </summary>
        /// <exception cref="NotImplementedException">
        /// if not implemented we will stop
        /// </exception>
        public void Stop()
        {
            if (!Configured)
            {
                throw new WebPushException("PushComponent is not properly Configured. NFFComponent::Stop() Aborted");
            }

            if (!enabled)
            {
                throw new WebPushException("PushComponent is not properly Configured. NFFComponent::Stop() Aborted");
            }

            Logger.Debug(this.pollTimer.Enabled == false ? "pollTimer is disabled" : "pollTimer is Enabled");

            const double Epsilon = 0;
            Logger.Debug(
                Math.Abs(this.pollTimer.Interval - 5000) < Epsilon
                    ? "pollTimer has an interval of 5000 milliseconds"
                    : "pollTimer DOES NOT have an interval of 5000 milliseconds");

            Logger.Debug(this.ErrorRetry ? "We are in ErrorRetry mode!" : "We are NOT in ErrorRetry mode!");

            Logger.Debug(
                this.pollStyle == PollStyle.FileSystemWatch
                    ? "pollStyle is of style FileSystemWatch"
                    : "pollStyle is NOT of style FileSystemWatch");

            if (!pollTimer.Enabled
                && (ErrorRetry || pollStyle != PollStyle.FileSystemWatch || Math.Abs(this.pollTimer.Interval - 5000) < Epsilon))
            {
                Logger.InfoFormat("Waiting for instance to complete work. Job {0}", InstanceName);
            }

            // Signal Stop
            _stopEvent.Set();

            if (!_busyEvent.WaitOne(30000))
            {
                Logger.InfoFormat(
                    "Instance did not complete properly. Data may be inconsistent. Job: {0}", InstanceName);
            }

            componentState = ComponentState.Halted;

            // Kill polling
            pollTimer.Stop();
        }

        /// <summary>
        /// The poll timer_ elapsed.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void pollTimer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            ThreadContext.Properties["JOBNAME"] = InstanceName;
            Logger.DebugFormat("PushComponent::pollTimer_Elapsed() hit");

            // Reset flags and pollers
            _busyEvent.WaitOne();
            pollTimer.Stop();

            try
            {
                Logger.Debug("InputFolder for " + InstanceName + " : " + FileInputFolder);

                // Process any wating files
                List<string> files =
                    new List<string>(
                        Directory.GetFiles(
                            FileInputFolder,
                            FileFilter,
                            IncludeSubdirs ? SearchOption.AllDirectories : SearchOption.TopDirectoryOnly));

                // Some logging
                Logger.DebugFormat("PushComponent::pollTimer_Elapsed() Convertion items scheduled: {0}", files.Count);

                // I need to have an Debug-flag here
                if (ConfigurationManager.AppSettings["Debug"].ToUpper() == "TRUE")
                {
                    foreach (string stringFiles in files)
                    {
                        Logger.Debug("Files to be converted are: " + stringFiles);
                    }
                }

                // Enable the event listening
                if (pollStyle == PollStyle.FileSystemWatch && !ErrorRetry)
                {
                    Logger.Debug("Setting filesWatcher.EnableRaisingEvents to true");
                    filesWatcher.EnableRaisingEvents = true;
                }

                if (files.Count > 0)
                {
                    // Push the files
                    HttpPush httpPush = new HttpPush
                                            {
                                                Host = PushTarget,
                                                PortNumber = PushPort,
                                                Scheme = PushProtocol,
                                                DonePath = FileDoneFolder,
                                                ErrorPath = FileErrorFolder
                                            };
                    Logger.Debug("Push the files to: " + PushTarget);
                    httpPush.ReadXmlDocument(files);
                }
            }
            catch (Exception ex)
            {
                Logger.Error("PushComponent::pollTimer_Elapsed() error: " + ex.Message, ex);
            }

            // Check error state
            if (ErrorRetry)
            {
                filesWatcher.EnableRaisingEvents = false;
            }

            // Restart
            pollTimer.Interval = Interval * 1000;
            if (pollStyle != PollStyle.FileSystemWatch
                || (pollStyle == PollStyle.FileSystemWatch && filesWatcher.EnableRaisingEvents == false))
            {
                pollTimer.Start();
            }

            _busyEvent.Set();
        }

        /// <summary>
        /// The files watcher_ created.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void filesWatcher_Created(object sender, FileSystemEventArgs e)
        {
            ThreadContext.Properties["JOBNAME"] = InstanceName;
            Logger.DebugFormat("PushComponent::filesWatcher_Created() hit with file data: {0}", e.FullPath);

            if (PollDelay > 0)
            {
                int pollTime = PollDelay;
                Logger.Debug("We are waiting: " + pollTime + " Seconds before starting to push!");

                // We take the value and multiply with thousand - to get the seconds... 
                Thread.Sleep(pollTime);
            }

            try
            {
                // Wait for the busy state
                _busyEvent.WaitOne();

                // Adding files to the list and then sending that list to the converter
                // @done: Create a method that takes string as argument also
                // @done: change XMLToNITFConverter so that it does not loop, but does only one file at the time
                List<string> files = new List<string> { e.FullPath };

                HttpPush httpPush = new HttpPush { Host = PushTarget, PortNumber = PushPort, Scheme = PushProtocol, DonePath = FileDoneFolder, ErrorPath = FileErrorFolder };
                httpPush.ReadXmlDocument(files);

                // Reset event
                _busyEvent.Set();
            }
            catch (Exception ex)
            {
                Logger.Debug("Something happened while doing filesWatcher_Created!", ex);
            }
        }
    }
}