﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="MainServiceComponent.cs" company="NTB">
//   NTB
// </copyright>
// --------------------------------------------------------------------------------------------------------------------
namespace NTB.WebPush.Components
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Configuration;
    using System.IO;
    using System.Linq;
    using System.ServiceModel;
    using System.Threading;
    using System.Xml;

    // Adding support for logging
    using log4net;

    // Adding enums
    using NTB.WebPush.Components.Enums;

    // Adding interface reference
    using NTB.WebPush.Components.Interfaces;

    // Adding support for logging
    using NTB.WebPush.Utilities;

    /// <summary>
    /// The main service component.
    /// </summary>
    public partial class MainServiceComponent : Component
    {
        #region Instance variables and control data

        /// <summary>
        /// Name of the file that contains the job configuration set
        /// </summary>
        /// <remarks>The name of the job config XML file is stored here.</remarks>
        protected string ConfigSet = string.Empty;

        /// <summary>
        /// Enable watching the config set for changes and reconfigure at runtime
        /// </summary>
        /// <remarks>config setting that defines if we are wathcing the config set for changes/auto reconfigure or not.</remarks>
        protected bool WatchConfigSet = false;

        /// <summary>
        /// List of currently Configured jobs
        /// </summary>
        /// <remarks>Internal Dictionary to keep track of running worker jobs.</remarks>
        protected Dictionary<string, IBaseWebPush> JobInstances =
            new Dictionary<string, IBaseWebPush>();

        /// <summary>
        /// Notification handler service host
        /// </summary>
        /// <remarks>
        /// This si the actuall service host that controls the notification object <see>
        ///                                                                            <cref>ewsNotify</cref>
        ///                                                                        </see>
        /// </remarks>
        protected ServiceHost ServiceHost = null;

        #endregion

        /// <summary>
        /// The logger.
        /// </summary>
        private static readonly ILog Logger;

        /// <summary>
        /// Initializes static members of the <see cref="MainServiceComponent"/> class.
        /// </summary>
        static MainServiceComponent()
        {
            // Set up logger
            log4net.Config.XmlConfigurator.Configure();
            if (!LogManager.GetRepository().Configured)
            {
                log4net.Config.BasicConfigurator.Configure();
            }

            Logger = LogManager.GetLogger(typeof(MainServiceComponent));

            if (Thread.CurrentThread.Name == null)
            {
                Thread.CurrentThread.Name = "MainServiceComponent";
            }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="MainServiceComponent"/> class.
        /// </summary>
        public MainServiceComponent()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="MainServiceComponent"/> class.
        /// </summary>
        /// <param name="container">
        /// The container.
        /// </param>
        public MainServiceComponent(IContainer container)
        {
            container.Add(this);

            InitializeComponent();
        }

        /// <summary>
        /// The configure.
        /// </summary>
        public void Configure()
        {
            try
            {
                // NDC.Push("CONFIG");
                ThreadContext.Stacks["NDC"].Push("CONFIG");

                // Read params 
                ConfigSet = ConfigurationManager.AppSettings["ConfigurationSet"];
                WatchConfigSet = Convert.ToBoolean(ConfigurationManager.AppSettings["WatchConfiguration"]);

                // Logging
                Logger.InfoFormat("{0} : {1}", "ConfigurationSet", ConfigSet);
                Logger.InfoFormat("{0} : {1}", "WatchConfiguration", WatchConfigSet);

                // Load configuration set
                XmlDocument config = new XmlDocument();
                config.Load(ConfigSet);

                // Setting up the watcher
                configFileWatcher.Path = Path.GetDirectoryName(ConfigSet);
                configFileWatcher.Filter = Path.GetFileName(ConfigSet);

                // Clearing all jobInstances before we populate them again. 
                JobInstances.Clear();

                // Creating Push Components
                XmlNodeList nodes =
                    config.SelectNodes("/ComponentConfiguration/PushComponents/PushComponent[@Enabled='True']");
                if (nodes != null)
                {
                    Logger.InfoFormat("PushComponent job instances found: {0}", nodes.Count);

                    foreach (XmlNode nd in nodes)
                    {
                        IBaseWebPush pushComponent = new PushComponent();
                        pushComponent.Configure(nd);

                        Logger.Debug("Adding " + pushComponent.InstanceName);
                        JobInstances.Add(pushComponent.InstanceName, pushComponent);
                    }
                }
            }
            catch (Exception exception)
            {
                Logger.Error(exception);
                Mail.SendException(exception);
            }
            finally
            {
                // ThreadContext.Stacks["NDC"].Pop();
                NDC.Pop();
            }
        }

        /// <summary>
        /// Starts the main service instance.
        /// </summary>
        /// <remarks>
        /// The function will set up polling and events to Start all processing jobs.
        /// </remarks>
        public void Start()
        {
            NDC.Push("START");

            try
            {
                // Start instances
                Logger.Debug("Number of jobs to start: " + JobInstances.Count.ToString());

                // Looping jobs
                foreach (
                    KeyValuePair<string, IBaseWebPush> kvp in JobInstances.Where(kvp => kvp.Value.Enabled))
                {
                    Logger.Info("Starting " + kvp.Value.InstanceName);
                    kvp.Value.Start();
                }

                // Starting maintenance
                Logger.Info("Starting maintenance timer");
                maintenanceTimer.Start();

                // Watch the config file
                configFileWatcher.EnableRaisingEvents = WatchConfigSet;
            }
            catch (Exception ex)
            {
                Logger.Fatal("An error has occured. Could not Start service", ex);
                Logger.Fatal(ex.StackTrace);
                Mail.SendException(ex);
            }
            finally
            {
                NDC.Pop();
            }
        }

        /// <summary>
        /// The pause.
        /// </summary>
        public void Pause()
        {
            maintenanceTimer.Stop();
            configFileWatcher.EnableRaisingEvents = false;

            // Stop instances
            foreach (KeyValuePair<string, IBaseWebPush> kvp in JobInstances.Where(kvp => kvp.Value.Enabled))
            {
                Logger.Info("Stopping " + kvp.Value.InstanceName);
                kvp.Value.Stop();
            }
        }

        /// <summary>
        /// The stop.
        /// </summary>
        public void Stop()
        {
            Pause();

            // Kill notification service handler
            if (ServiceHost == null)
            {
                return;
            }

            ServiceHost.Close();
            ServiceHost = null;
        }

        /// <summary>
        /// The maintenance timer_ elapsed.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void maintenanceTimer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            MDC.Set("JOBNAME", "MainWorkerJob");

            Logger.Debug("In maintenanceTimer_Elapsed");

            /*
             * Not sure what this shall do yet, 
             * but I would believe that getting some data would be an idea...
             */
            try
            {
                maintenanceTimer.Stop();

                // We are checking if the jobs are doing what they are supposed to do
                foreach (KeyValuePair<string, IBaseWebPush> kvp in
                    JobInstances.Where(kvp => kvp.Value.Enabled)
                                .Where(kvp => kvp.Value.ComponentState == ComponentState.Halted))
                {
                    Logger.Info("The component is in Halted state, even though it is Enabled");
                    Logger.Debug("We are starting the component!");

                    kvp.Value.Start();
                }

                maintenanceTimer.Start();

                // Watch the config file
                if (!configFileWatcher.EnableRaisingEvents)
                {
                    configFileWatcher.EnableRaisingEvents = WatchConfigSet;
                }
            }
            catch (Exception ex)
            {
                Logger.ErrorFormat(
                    "MainServiceComponent::maintenanceTimer_Elapsed() failed to renew PushSubscription: " + ex.Message,
                    ex);
                Mail.SendException(ex);
            }
        }

        /// <summary>
        /// The config file watcher_ changed.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void configFileWatcher_Changed(object sender, FileSystemEventArgs e)
        {
            ThreadContext.Properties["JOBNAME"] = "MainServiceWorker";

            // Outer-try-final to prevent dupe events
            try
            {
                // Stopping file watcher
                configFileWatcher.EnableRaisingEvents = false;
                Logger.Info("MainServiceComponent::configFileWatcher_Changed() hit. Will reconfigure.");

                ThreadContext.Stacks["NDC"].Push("RECONFIGURE");

                // Pause everything
                Pause();

                // Give it a little break
                Thread.Sleep(5000);

                // Reconfigure
                ConfigurationManager.RefreshSection("applicationSettings");
                ConfigurationManager.RefreshSection("appSection");
                Configure();

                Start();
            }
            catch (Exception ex)
            {
                Logger.Fatal("NTB WebPush Service reconfiguration failed - TERMINATING!", ex);
                Mail.SendException(ex);
                throw;
            }
            finally
            {
                ThreadContext.Stacks["NDC"].Pop();
                configFileWatcher.EnableRaisingEvents = WatchConfigSet;
            }
        }
    }
}