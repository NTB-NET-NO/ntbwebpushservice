﻿// -----------------------------------------------------------------------
// <copyright file="HttpPush.cs" company="NTB">
// NTB
// </copyright>
// -----------------------------------------------------------------------

namespace NTB.WebPush.Components
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Net;
    using System.Text;
    using System.Xml;

    // Adding support for logging
    using log4net;

    /// <summary>
    /// TODO: Update summary.
    /// </summary>
    public class HttpPush
    {
        /// <summary>
        /// Static logger
        /// </summary>
        private static readonly ILog Logger = LogManager.GetLogger(typeof(PushComponent));

        /// <summary>
        /// Initializes a new instance of the <see cref="HttpPush"/> class.
        /// </summary>
        public HttpPush()
        {
            // Set up logger
            log4net.Config.XmlConfigurator.Configure();
            if (!LogManager.GetRepository().Configured)
            {
                log4net.Config.BasicConfigurator.Configure();
            }
        }

        /// <summary>
        /// Gets or sets the scheme.
        /// </summary>
        public string Scheme { get; set; }

        /// <summary>
        /// Gets or sets the host.
        /// </summary>
        public string Host { get; set; }

        /// <summary>
        /// Gets or sets the port number.
        /// </summary>
        public int PortNumber { get; set; }

        /// <summary>
        /// Gets or sets the error path.
        /// </summary>
        public string ErrorPath { get; set; }

        /// <summary>
        /// Gets or sets the done path.
        /// </summary>
        public string DonePath { get; set; }

        /// <summary>
        /// The read xml document.
        /// </summary>
        /// <param name="files">
        /// The files.
        /// </param>
        public void ReadXmlDocument(List<string> files)
        {
            foreach (string file in files)
            {
                try
                {
                    Logger.Info("Reading document: " + file);
                    XmlDocument xmlDocument = new XmlDocument();
                    xmlDocument.Load(file);

                    string document;

                    Logger.Debug("Creating string object to read the content of the file");
                    XmlWriterSettings writerSettings = new XmlWriterSettings
                        {
                            Indent = true,
                            IndentChars = "     ",
                            NewLineChars = "\r\n",
                            NewLineHandling = NewLineHandling.Replace,
                            Encoding = Encoding.UTF8
                        };

                    // Turning the xmlDocument object into a string
                    using (var stringWriter = new StringWriter())
                    using (var xmlTextWriter = XmlWriter.Create(stringWriter, writerSettings))
                    {
                        xmlDocument.WriteTo(xmlTextWriter);
                        xmlTextWriter.Flush();
                        document = stringWriter.GetStringBuilder().ToString();
                        document = document.Replace("iso-8859-1", "UTF-8");
                    }

                    Logger.Debug("length of string: " + document.Length);

                    // Create a request using a URL that can receive a post. 
                    WebRequest request = WebRequest.Create("http://www.trondhuso.no/httppush/index.php");
                    
                    // Set the Method property of the request to POST.
                    request.Method = "POST";
                    
                    // Create POST data and convert it to a byte array.
                    string postData = document;
                    byte[] byteArray = Encoding.UTF8.GetBytes(postData);
                    
                    // Set the ContentType property of the WebRequest.
                    request.ContentType = "application/x-www-form-urlencoded";
                    
                    // Set the ContentLength property of the WebRequest.
                    request.ContentLength = byteArray.Length;
                    
                    // Get the request stream.
                    Stream dataStream = request.GetRequestStream();
                    
                    // Write the data to the request stream.
                    dataStream.Write(byteArray, 0, byteArray.Length);
                    
                    // Close the Stream object.
                    dataStream.Close();
                    
                    // Get the response.
                    WebResponse response = request.GetResponse();
                    
                    // Display the status.
                    Console.WriteLine(((HttpWebResponse)response).StatusDescription);
                    
                    // Get the stream containing content returned by the server.
                    dataStream = response.GetResponseStream();
                    
                    // Open the stream using a StreamReader for easy access.
                    if (dataStream != null)
                    {
                        StreamReader reader = new StreamReader(dataStream);
                    
                        // Read the content.
                        string responseFromServer = reader.ReadToEnd();
                    
                        // Display the content.
                        Logger.Info(responseFromServer);
                    
                        // Clean up the streams.
                        reader.Close();

                        dataStream.Close();
                    }

                    response.Close();
                }
                catch (Exception exception)
                {
                    Logger.Error(exception.Message);
                    Logger.Error(exception.StackTrace);

                    // Todo: Move to error folder
                    FileInfo fileInfo = new FileInfo(file);
                    string filename = fileInfo.Name;

                    FileInfo fileInfoDone = new FileInfo(ErrorPath + @"\" + filename);
                    if (fileInfoDone.Exists)
                    {
                        fileInfoDone.Delete();
                    }
                    fileInfo.MoveTo(ErrorPath + @"\" + filename);
                }
                finally
                {
                    // Todo: Move to done folder
                    FileInfo fileInfo = new FileInfo(file);
                    string filename = fileInfo.Name;
                    FileInfo fileInfoDone = new FileInfo(DonePath + @"\" + filename);
                    if (fileInfoDone.Exists)
                    {
                        fileInfoDone.Delete();
                    }
                    fileInfo.MoveTo(DonePath + @"\" + filename);
                }
            }
        }
    }
}
