﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="MainWebPushService.Designer.cs" company="NTB">
//   NTB
// </copyright>
// <summary>
//   Defines the MainWebPushService type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace NTB.WebPush.Service
{
    /// <summary>
    /// The main web push service.
    /// </summary>
    public partial class MainWebPushService
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// The _startup config timer.
        /// </summary>
        private System.Timers.Timer _startupConfigTimer;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this._startupConfigTimer = new System.Timers.Timer();
            ((System.ComponentModel.ISupportInitialize)(this._startupConfigTimer)).BeginInit();
            // 
            // _startupConfigTimer
            // 
            this._startupConfigTimer.Interval = 2000D;
            this._startupConfigTimer.Elapsed += new System.Timers.ElapsedEventHandler(this._startupConfigTimer_Elapsed);
            // 
            // MainWebPushService
            // 
            this.ServiceName = "Service1";
            ((System.ComponentModel.ISupportInitialize)(this._startupConfigTimer)).EndInit();

        }

        #endregion
    }
}
