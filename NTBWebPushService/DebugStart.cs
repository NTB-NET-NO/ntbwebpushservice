﻿// -----------------------------------------------------------------------
// <copyright file="DebugStart.cs" company="NTB">
// NTB
// </copyright>
// -----------------------------------------------------------------------

namespace NTB.WebPush.Service
{
    using System;
    using System.Collections.Generic;
    using System.Windows.Forms;

    /// <summary>
    /// Entry point when compiling for debugging as a WinForm
    /// </summary>
    public static class DebugStart
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        public static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new FormDebug());

        }
    }
}
