﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ServiceStart.cs" company="NTB">
//   NTB
// </copyright>
// <summary>
//   The service start.
// </summary>
// --------------------------------------------------------------------------------------------------------------------



namespace NTB.WebPush.Service
{
    using System.ServiceProcess;

    /// <summary>
    /// The service start.
    /// </summary>
    internal static class ServiceStart
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        private static void Main()
        {
            ServiceBase[] servicesToRun = new ServiceBase[] { new MainWebPushService() };
            ServiceBase.Run(servicesToRun);
        }
    }
}