﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="MainWebPushService.cs" company="NTB">
//   NTB
// </copyright>
// <summary>
//   Defines the MainWebPushService type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace NTB.WebPush.Service
{
    using System;
    
    using System.ServiceProcess;

    using log4net;

    using NTB.WebPush.Components;


    /// <summary>
    /// The main web push service.
    /// </summary>
    public partial class MainWebPushService : ServiceBase
    {
        /// <summary>
        /// The service.
        /// </summary>
        protected MainServiceComponent Service = null;

        /// <summary>
        /// The _logger.
        /// </summary>
        private static ILog logger = LogManager.GetLogger(typeof(MainWebPushService));

        /// <summary>
        /// Initializes a new instance of the <see cref="MainWebPushService"/> class.
        /// </summary>
        public MainWebPushService()
        {
            // log4net.Config.XmlConfigurator.Configure();
            // MDC.Set("JOBNAME", "NTB.SportsData.Service");
            ThreadContext.Properties["JOBNAME"] = "NTB.WebPush.Service";
            log4net.Config.XmlConfigurator.Configure();

            logger = LogManager.GetLogger(typeof(MainWebPushService));

            logger.Info("In MainWebPushService - starting up");

            // Creating the new MainServiceComponent
            Service = new MainServiceComponent();

            // Initialising this main component
            InitializeComponent();
        }

        /// <summary>
        /// The on start.
        /// </summary>
        /// <param name="args">
        /// The args.
        /// </param>
        protected override void OnStart(string[] args)
        {
            try
            {
                logger.Info("NTB WebPush Service starting...");
                _startupConfigTimer.Start();
            }
            catch (Exception ex)
            {
                logger.Fatal("NTB WebPush Service DID NOT START properly - Terminating!", ex);
                throw;
            }
        }

        /// <summary>
        /// The on stop.
        /// </summary>
        protected override void OnStop()
        {
            try
            {
                logger.Info("Stopping service");
                Service.Stop();

                _startupConfigTimer.Enabled = false;
            }
            catch (Exception ex)
            {
                logger.Fatal("NTB WebPush Service did not stop properly - Terminating!", ex);
                throw;
            }
        }

        /// <summary>
        /// The _startup config timer_ elapsed.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void _startupConfigTimer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            _startupConfigTimer.Stop();

            try
            {
                Service.Configure();
                Service.Start();
                logger.Info("MainSportsDataService Started!");
            }
            catch (Exception ex)
            {
                logger.Fatal("MainSportsDataService DID NOT START properly - TERMINATING!", ex);

                throw;
            }
        }
    }
}