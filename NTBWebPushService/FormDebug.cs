﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="FormDebug.cs" company="NTB">
//   NTB
// </copyright>
// <summary>
//   Defines the FormDebug type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace NTB.WebPush.Service
{
    using System;
    using System.Windows.Forms;

    using log4net;

    using NTB.WebPush.Components;

    /// <summary>
    /// The form debug.
    /// </summary>
    public partial class FormDebug : Form
    {
        /// <summary>
        /// The actual service object
        /// </summary>
        /// <remarks>Does the actuall work</remarks>
        protected MainServiceComponent Service = null;

        /// <summary>
        /// Creating the logger object so we can log stuff
        /// </summary>
        private static ILog logger = LogManager.GetLogger(typeof(FormDebug));

        /// <summary>
        /// Initializes a new instance of the <see cref="FormDebug"/> class.
        /// </summary>
        public FormDebug()
        {
            ThreadContext.Properties["JOBNAME"] = "WebPush Service-Debug";
            log4net.Config.XmlConfigurator.Configure();
            logger = LogManager.GetLogger(typeof(FormDebug));

            this.Service = new MainServiceComponent();

            InitializeComponent();
        }

        /// <summary>
        /// The form debug_ load.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void FormDebug_Load(object sender, EventArgs e)
        {
            try
            {
                logger.Info("NTB WebPush Service DEBUGMODE starting...");
                _startupConfigTimer.Start();
            }
            catch (Exception ex)
            {
                logger.Fatal("NTB WebPush Service DEBUGMODE DID NOT START properly", ex);
            }
        }

        /// <summary>
        /// The _startup config timer_ tick.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void _startupConfigTimer_Tick(object sender, EventArgs e)
        {
            // Kill the timer
            _startupConfigTimer.Stop();

            // And configure
            try
            {
                this.Service.Configure();
                this.Service.Start();
                logger.Info("NTB WebPush Service DEBUGMODE started");
            }
            catch (Exception ex)
            {
                logger.Fatal("NTB WebPush Service DEBUGMODE DID NOT START properly", ex);
            }
        }

        /// <summary>
        /// The form debug_ form closing.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void FormDebug_FormClosing(object sender, FormClosingEventArgs e)
        {
            try
            {
                this.Service.Stop();
                logger.Info("NTB WebPush Service DEBUGMODE stopped");
            }
            catch (Exception ex)
            {
                logger.Fatal("NTB WebPush Service DEBUGMODE DID NOT STOP properly", ex);
            }
        }
    }
}
